Dot Net Web Server
==================
This is a simple web server implemented in .Net/C#. It has plugin/request routing mechanism, so it should be fun to use. Including plugin is an OX game.

Required software
-----------------
1. .Net Core https://www.microsoft.com/net/core
2. Visual Studio Code https://code.visualstudio.com/ (optional)

Please make sure you fork the git repo first, don't try to push to my repo!

Step to build repo
------------------
1. Check out the code (your code!!!, dont check out from my repo)
2. run " dotnet restore"  to check dependency
3. run " dotnet build " to check if you can build the code
4. run " dotnet ef migrations add Users"  to add migration plan
5. run " dotnet ef database update " to create database tables
6. run " dotnet build" again to compile all models/ORM
7. run " dotnet run"  to start the project.